import spock.lang.Specification
import starbucks.DrinkService
import starbucks.DrinksController
import starbucks.exceptions.DrinkNotFoundException
import starbucks.model.Drink

/**
 * Created by mbritez on 9/05/16.
 */
class DrinkController extends Specification {

    private DrinksController controller
    private DrinkService mockedService

    def setup(){
        this.mockedService = Mock(DrinkService)
        this.controller = new DrinksController(mockedService)
    }

    def "test get drink"(){
        setup:
        Drink mockedDrink = Mock()
        this.mockedService.get(_ as Long) >> mockedDrink

        when:
        def result = this.controller.get(1L)

        then:
        result
        result == mockedDrink
    }

    def "test non existence drink"(){
        setup:
        this.mockedService.get(1L) >> {throw new DrinkNotFoundException(1L)}

        when:
        def result = this.controller.get(1L)

        then:
        thrown(DrinkNotFoundException)
        !result
    }

    def "test create Drink"(){
    	setup:
    	Drink mockedDrink = Mock(){ getId() >> 1L }
    	this.mockedService.create(mockedDrink) >> {1L}
    	
    	when:
    	def id = this.controller.create(mockedDrink)
    	
    	then:
    	id == 1L
    	id == mockedDrink.getId()
    }
}
