import spock.lang.Specification
import starbucks.DrinkService
import starbucks.exceptions.DrinkNotFoundException
import starbucks.model.Drink

class DrinkService extends Specification {

	private DrinkService service;
	
	def setup(){
	        service = new DrinkService();
	}
	
	def "test get Drink"(){
		setup:
		Drink mockedDrink = Mock()
	
		when:
		service.get(1L) 
	
		then:
		result == mockedDrink
	}
}