package starbucks.exceptions;

public class DrinkNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 5548519841610031896L;
	
	private Long id;
	
	public DrinkNotFoundException(Long id){
		this.id = id;
	}
	
	public String getErrorMessage(){
		return String.format("Drink with id: %s, not found", this.id);
	}

}
