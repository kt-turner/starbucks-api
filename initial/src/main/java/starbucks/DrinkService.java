package starbucks;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.springframework.stereotype.Service;

import starbucks.exceptions.DrinkNotFoundException;
import starbucks.model.Drink;

@Service
public class DrinkService {
	
	private Map<Long, Drink> store;
	
	public DrinkService(){
		this.store = new HashMap<>();
	}
	
	public Drink get(Long id) {
		return Optional
				.ofNullable(this.store.get(id))
				.orElseThrow(() -> new DrinkNotFoundException(id));
	}
	
	public Long create(Drink drink){
		drink.setId(Long.valueOf(store.size()+1));
		store.put(drink.getId(), drink);
		return drink.getId();
	}
	
	public void update(Long id, Drink drink){
		Drink storedDrink = this.get(id);
		storedDrink.setName(drink.getName());
		storedDrink.setDescription(drink.getDescription());
	}	

	public Collection<Drink> list() {
		return this.store.values();
	}
	
	public void delete(Long id){
		Drink storedDrink = this.store.get(id);
		store.remove(storedDrink.getId(), storedDrink);
	}
}
