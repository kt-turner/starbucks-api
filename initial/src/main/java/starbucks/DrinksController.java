package starbucks;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import starbucks.exceptions.DrinkNotFoundException;
import starbucks.model.Drink;
import starbucks.model.ErrorResponse;

@RestController
@RequestMapping("/drinks")
public class DrinksController {
	
	private DrinkService service;
	
	@Autowired
	public DrinksController(DrinkService service) {
		this.service = service;
	}
    
	@RequestMapping(value= "/{id}", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public Drink get(@PathVariable Long id) {
        return service.get(id);
    }
    
    @RequestMapping(method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public Collection<Drink> list() {
        return service.list();
    }
	
	@RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.CREATED)
    public Long create(@RequestBody Drink drink) {
        return service.create(drink);
    }
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void put(@PathVariable Long id, @RequestBody Drink drink){
		service.update(id, drink);
	}
	
	@RequestMapping(value= "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        service.delete(id);
    }
	
	@ExceptionHandler(DrinkNotFoundException.class)
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public ErrorResponse handle(DrinkNotFoundException e){
		return new ErrorResponse(e.getErrorMessage());
	}
}
